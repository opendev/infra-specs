::

  Copyright (c) 2021 Acme Gating, LLC

  This work is licensed under a Creative Commons Attribution 3.0
  Unported License.
  http://creativecommons.org/licenses/by/3.0/legalcode

=======================
Support Matrix for Chat
=======================

Storyboard: https://storyboard.openstack.org/#!/story/2009034

We just switched IRC networks from Freenode to OFTC.  This was done
quickly because remaining on Freenode was untenable due to recent
changes, and the OpenDev community had an existing plan prepared to
move to OFTC should such a situation arise.

Now that the immediate issue is addressed, the Zuul community has
consulted both internally and externally and would like to host its
community chat in Matrix.

Problem Description
===================

Here are some concerns that affect us as a community:

* Some users like to stay connected all the time so they can read
  messages from when they are away.

* Others are only interested in connecting when they have something to
  say.

* On Freenode, nick registration was required to join OpenDev related
  channels in order to mitigate spam.  Currently that is not the case
  with OFTC, but we can't guarantee that will hold.

* Some users prefer simple text-based clients.

* Others prefer rich messaging and browser or mobile clients.

* Projects rely on the OpenDev bots: gerritbot, meetbot, eavesdrop,
  and statusbot.

* OpenDev projects collaborate with each other in various OFTC
  channels.  They also collaborate with folks in other communities in
  libera.chat channels.

* Users must be able to access our chat using Free and Open-Source
  Software.

* The software running the chat system itself should be Free and
  Open-Source as well if possible.  Both of these are natural
  extensions of the Open Infrastructure community's Four Opens, as
  well as OpenDev's mantra that Free Software needs Free Tools.

Benefits Offered by Matrix
--------------------------

* The Matrix architecture associates a user with a "homeserver", and
  that homeserver is responsible for storing messages in all of the
  rooms the user is present.  This means that every Matrix user has
  the ability to access messages received while their client is
  disconnected.  Users don't need to set up separate "bouncers".

* Authentication happens with the Matrix client and homeserver, rather
  than through a separate nickserv registration system.  This process
  is familiar to all users of web services, so should reduce barriers
  to access for new users.

* Matrix has a wide variety of clients available, including the
  Element web/desktop/mobile clients, as well as the weechat-matrix
  plugin.  This addresses users of simple text clients and rich media.

* Bots are relatively simple to implement with Matrix.

* The Matrix community is dedicated to interoperability.  That drives
  their commitment to open standards, open source software, federation
  using Matrix itself, and bridging to other communities which
  themselves operate under open standards.  That aligns very well with
  our four-opens philosophy, and leads directly to the next point:

* Bridges exist to OFTC, libera.chat, and, at least for the moment,
  Freenode.  That means that any of our users who have invested in
  establishing a presence in Matrix can relatively easily interact
  with communities who call those other networks home.

* End-to-end encrypted channels for private chats.  While clearly the
  public channels are our main concern, and they will be public and
  unencrypted, the ability for our community members to have ad-hoc
  chats about sensitive matters (such as questions which may relate to
  security) is a benefit.  If Matrix becomes more widely used such
  that employees of companies feel secure having private chats in the
  same platform as our public community interactions, we all benefit
  from the increased availability and accessibility of people who no
  longer need to split their attention between multiple platforms.

* The `Spaces <https://element.io/blog/spaces-the-next-frontier/>`_
  feature allows for easier discovery of available channels for a
  given community (see

Reasons to Move
---------------

Projects could continue to call their channels on OFTC home, and
individual users could still use Matrix on their own to obtain most of
those benefits by joining the portal room on the OFTC matrix.org
bridge.  Some reasons to move to a native Matrix room are:

* Eliminate a potential failure point.  If many/most of us are
  connected via Matrix and the bridge, then either a Matrix or an OFTC
  outage would affect us.

* Eliminate a source of spam.  Spammers find IRC networks very easy to
  attack.  Matrix is not immune to this, but it is more difficult.

* Isolate ourselves from OFTC-related technology or policy changes.
  For example, if we find we need to require registration to speak in
  channel, that would take us back to the state where we have to teach
  new users about nick registration.

* Elevating the baseline level of functionality expected from our chat
  platform.  By saying that our home is Matrix, we communicate to
  users that the additional functionality offered by the platform is
  an expected norm.  Rather than tailoring our interactions to the
  lowest-common-denominator of IRC, we indicate that the additional
  features available in Matrix are welcomed.

* Provide a consistent and unconfusing message for new users.  Rather
  than saying "we're on OFTC, use Matrix to talk to us for a better
  experience", we can say simply "use Matrix".

* Lead by example.  Because of the recent fragmentation in the Free
  and Open-Source software communities, Matrix is a natural way to
  frictionlessly participate in a multitude of communities.  Let's
  show people how that can work.

Alternatives
------------

All of the work to move to OFTC has been done, and for the moment at
least, the OFTC matrix.org bridge is functioning well.  OpenDev could
decide not to support the Zuul project in their effort to move to
Matrix.

OpenDev could also offer limited support by operating bots, but
without operating a homeserver.

Implementation
==============

Assignee(s)
-----------

  * Clark Boylan (Sign up for Element.io service on behalf of Foundation)
  * Tristan Cacqueray (Matrix gerritbot replacement)
  * TBD (statusbot replacement/update; corvus is fallback)
  * TBD (eavesdrop replacement/update; corvus is fallback)
  * TBD (meetbot replacement/update)

Gerrit Topic
------------

Use the gerrit topic ``matrix`` for changes related to this spec.

Work Items
----------

To support projects that want to use Matrix, OpenDev would do the
following:

* Create a homeserver to host our room and bots.  Technically, this is
  not necessary, but having a homeserver allows us more control over
  the branding, policy, and technology of our room.  It means we are
  isolated from policy decisions by the admins of matrix.org, and it
  fully utilizes the federated nature of the technology.

  The Foundation will subscribe to a hosted server run by Element.

  With Element taking care of maintenance and upgrades, the OpenDev
  sysadmins have one less potential risk.

  At this stage, we would not host any user accounts on the
  homeserver; it would only be used for hosting rooms and bot/admin
  accounts.

  The homeserver will be for `opendev.org`; so for example, a room for
  Zuul would be `#zuul:opendev.org`, and we might expect bot accounts
  like `@gerrit:opendev.org`.

* Ensure that the OpenDev service bots support matrix.  Zuul makes use
  of gerritbot, statusbot, and an eavesdrop service, so with only
  those implemented, it would be sufficient for the Zuul project to
  move.  A meeting bot could be implemented later for other projects
  to move.

  Note that any bot that can operate over the Matrix protocol can
  participate in Matrix or any IRC network, so OpenDev can support
  projects using Matrix and projects which remain on IRC with the same
  technology by implementing them using Matrix.

* Once the homeserver exists, we should create rooms.  A declarative
  git-driven process would be nice, or if we expect only one or a
  small number of rooms, we could have an admin account manually
  create them to start.

* Create instructions to tell users how to use Matrix.  These have
  already been `created`_ for the Zuul project.  Zuul currently
  recommends users join the #zuul OFTC channel via the Matrix bridge,
  so these instructions only need to be updated to change the name of
  the room.

* Announce the move, and retire the OFTC channel.

.. _created:
  https://review.opendev.org/796153

Potential Future Enhancements
-----------------------------

Once the OpenDev single-sign-on system is complete, It would be
possible for OpenDev and/or the Foundation to add a second homeserver
for the purpose of hosting end-user accounts.  This might be more
comfortable for new users who are joining Matrix at the behest of our
community.  But it also opens questions about identity and usage (the
same questions we would have to answer, for example, if we provided
email addresses to project participants), so much further discussion
would be needed if this was desired.

It is possible for OpenDev and/or the Foundation to run multiple
homeservers in multiple locations in order to aid users who may live
in jurisdictions with policy or technical requirements that prohibit
their accessing the matrix.org homeserver.

Because Matrix can interoperate with other messaging systems, it would
be possible for OpenDev to use its homeserver to host portal rooms to
other communities using other software (with their permission).  For
example, OpenDev could host a bridge to k8s slack, making it simpler
for contributors to communicate with both projects.

All of these, if they come to pass, would be significantly in the
future but they do illustrate some of the additional flexibility our
communities could obtain by using Matrix.


Repositories
------------

If OpenDev runs a homeserver, it will be configured in system-config.
We may need to modify or write new bots, so gerritbot at least may be
affected.  We may need new repos for new bots if we write them.

Servers
-------

We may run a homeserver; if so, we can probably run it on a 4GB VM to
start.

DNS Entries
-----------

If we run a homeserver, it will require DNS entries.

Regardless of whether we run one or use an Element-hosted homeserver,
we will place a file in the .well-known/ directory on opendev.org to
delegate "opendev.org" to the homeserver so that Matrix addresses
ending with ":opendev.org" are routed there.  No DNS entries are
required for this.

Documentation
-------------

New user documentation has already been written for Zuul, we can copy
that to the infra-manual.

Security
--------

In every respect, Matrix is more secure than IRC.  If we run a
homeserver ourselves we will be responsible for keeping it up to date,
which is not a responsibility we currently have with IRC.

Testing
-------

Testing for our bots can be considerably more authentic since we can
dynamically spin up a Matrix server in a gate test job.

Dependencies
============

None.
