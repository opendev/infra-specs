::

  Copyright 2020 Thierry Carrez

  This work is licensed under a Creative Commons Attribution 3.0
  Unported License.
  http://creativecommons.org/licenses/by/3.0/legalcode

==========
Xwiki farm
==========

This document describes setting up a wiki farm based on `Xwiki`_ to serve the
needs for lightweight, structured document publication (between etherpad
and git-driven formal documentation) within Opendev communities.

Problem Description
===================

Opendev serves multiple subcommunities. Some of them have expressed the need
for a wiki: a lightweight way to publish information while retaining clear
history and authorship. This is traditionally very helpful at early stages
of the formation of communities.

Our current response is to ask them to set up pages on the Mediawiki instance
on wiki.openstack.org. Beyond the obvious domain problem, this creates
additional issues:

* it increases our dependency on a difficult-to-maintain platform

* it makes it hard to structure information or search within a given community
  scope

Proposed Change
===============

I evaluated various solutions. `Xwiki`_ emerged as a good candidate:

* open source (although a bit open core)

* limited maintenance costs, includes `spam prevention tooling`_

* `"structured wiki"`_ approach, which limits stale pages

* pluggable into openstackID for authentication

* support for `subwikis`_ (wikis with a root document and search context which
  live under the same main wiki domain) allowing each community to have what
  looks like a separate wiki

`Xwiki.com`_, the company behind Xwiki, offers `free hosting`_ for open source
communities. That offer is for the basic version of their cloud service.
It includes up to 10 subwikis, unlimited users, 20Gb of storage, and limited
tech support. They agreed to include usage of a custom domain, as well as
manual configuration of OpenStackID authentication, although it would
technically not be covered in the free hosting offer.

As a proof-of-concept, we propose to accept the free hosting offer and set up
a wiki farm at wiki.opendev.org. Then we'd evaluate the software using a
guinea pig community.

The OSF is helping bootstrapping the OpenInfra labs community (formerly
known as Massachussets Open Cloud), a community of practice around operating
OpenStack clouds for academic networks. That community is interested in
using Opendev for project infrastructure, including Gerrit for managing code
repositories. At those early stages they need a mailing-list (already set up
at lists.opendev.org) and a wiki to share information.

The proposed plan is to create a subwiki for the Open Infra Labs community,
and after some time evaluate how successful that setup is, before thinking
about next steps (offer subwikis to other communities, hosting ourselves).

.. _`Xwiki`: https://xwiki.org
.. _`spam prevention tooling`: https://www.xwiki.org/xwiki/bin/view/Documentation/AdminGuide/AntiSpam
.. _`"structured wiki"`: https://www.xwiki.org/xwiki/bin/view/Documentation/UserGuide/Features/ContentOrganization/
.. _`subwikis`: https://www.xwiki.org/xwiki/bin/view/Documentation/UserGuide/Features/ContentOrganization/WikiVsNestedPages/
.. _`Xwiki.com`: https://xwiki.com
.. _`free hosting`: https://xwiki.com/en/Blog/XWikiCloudFreeForOpenSource

Alternatives
------------

Doubling down on Mediawiki
~~~~~~~~~~~~~~~~~~~~~~~~~~

We could host separate Mediawikis for each community under OpenDev, based on
the puppet modules that we use to maintain wiki.openstack.org. However
mediawiki has not been very pleasant to maintain, and we tried to reduce our
exposure to it rather than increase it. Xwiki's structured pages model also
seems more adapted to our needs than Mediawiki's flat model.

Enabling gitea-wiki
~~~~~~~~~~~~~~~~~~~

Gitea has a nice wiki feature (attached to specific repositories rather than
specific communities). Since we are already using Gitea, it would make sense
to enable that feature instead of relying on additional software.

However our gitea system is currently a set of independent setups, kept in
sync via git pushes from gerrit. To enable the wiki feature, you need to be
able to write to the git repos within gitea (and keep that consistent across
the installation), which is still an unsolved problem at this point.
Additionally, you need to enable user management and authentication support
in Gitea, which is also an area nobody is planning to work on.

Direct Xwiki hosting
~~~~~~~~~~~~~~~~~~~~

We could host Xwiki directly, rather than rely on Xwiki.com cloud hosting.
That would have the benefit of not appearing to endorse an open core
company, and keep our data and access log under our direct control. However,
the open core model followed by Xwiki.com relies on a relatively sane
demarcation between xwiki code and "pro" plugins, and Xwiki.com being a
French company, they have to abide to pretty drastic data collection,
retention and communication laws. Overall, the benefits of being able to
outsource hosting and maintenance of the platform, as well as the immediate
availability of the solution, justifies the positive trade-off.

Shall the proof-of-concept be successful, or shall we ever exceed the
10-subwiki limit, we would consider self-hosting the solution. Usage of a
custom domain reduced the impact of a future transition.

Open questions
--------------

Authentication
~~~~~~~~~~~~~~

Opendev did not make a clear decision on single-sign-on. As such,
wiki.opendev.org would be set up with OpenStackID integration to avoid
creating another set of crednetials, or increasing our UbuntuOne technical
debt. Should we migrate to another SSO solution in the future, accounts for
the proof-of-concept can easily be recreated if that ends up being necessary.

However this issue should be tackled before we expand to other subwikis, and
certainly before we ever consider moving wiki.openstack.org content.

SSL
~~~

It is unclear at this moment how SSL certificate would be handled for
wiki.opendev.org. This should be clarified with Xwiki support as we set up
the instance.


Implementation
==============

Assignee(s)
-----------

Primary assignee:
  ttx

Gerrit Topic
------------

Use Gerrit topic "xwiki-farm" for all patches related to this spec.

.. code-block:: bash

    git-review -t xwiki-farm

Work Items
----------

* Set up main instance with OpenStackID integration, SSL.
* Set up openinfralabs subwiki and open it up in that community
* Evaluate after a few months of usage

Repositories
------------

None.

Servers
-------

None.

DNS Entries
-----------

wiki.opendev.org will have to be created a point to wherever XWiki.com
asks us to point.

Documentation
-------------

Documentation of the wiki set up should be added.

Security
--------

Wiki is isolated from the rest of opendev systems. We should make sure we do
not accidentally leak profile information (by default Xwiki lists email
addresses and OIDC addresses in the autogenarted profile page, this needs to
be disabled).

Testing
-------

None.

Dependencies
============

None.
