::

  Copyright (C) 2020 Red Hat, Inc

  This work is licensed under a Creative Commons Attribution 3.0
  Unported License.
  http://creativecommons.org/licenses/by/3.0/legalcode

===============================================
Run an etherpad-focused video conference server
===============================================

https://storyboard.openstack.org/...

This is a proposal to run a self-hosted instance of jitsi-meet with
tight integration with our etherpad server to help facilitate remote
design sessions.

Problem Description
===================

The OpenDev community is soon looking at having its first virtual PTG
due to the current pandemic in addition to being strong advocates and
innovators in remote collaboration for years.

One of the key activities at PTGs are time-limited sessions where
contributors gather together and discuss future work with the help of
an etherpad to record notes, requirements, and work items from the
session.  We may be able to approximate this virtually using
jitsi-meet and etherpad.

(Another important activity involves beer afterwords; this spec does
not address that need.)

Proposed Change
===============

`Jitsi-meet`_ is a FLOSS video conferencing system.  It's based on
nice open standards like XMPP, but users don't need to know about
that.  Most users will interact with it via a browser using WEBRTC and
it will appear just like other popular web video conferencing systems.

Unlike others, however, it has built-in integration with etherpad, so
that we can configure it so that collaborative documentation editing
will take center stage, rather than a giant talking head (though that
is still an option for individual users if they wish).

We will call the service ``meetpad`` and run it at
``meetpad.opendev.org``.  Giving it a stronger individual brand may
help communicate the intended use and focus better, and help convey
how this is different than other video conferencing.

.. _Jitsi-meet: https://jitsi.org/jitsi-meet/

Alternatives
------------

We could do nothing, which will likely lead to users using either the
existing audio-only conferencing bridge, or far more likely, combining
proprietary video conferencing services in one window with
collaborative document editing in another.

We could work on vaccine research instead of developing software,
though that is outside of most our areas of expertise.  Even if we did
that, we'd still probably want video conferencing.

Implementation
==============

We will create a new VM and run the full jitsi-meet stack using
docker-compose.  It consists of 4 services: the web service, the video
bridge, the focus controller, and an XMPP server.  These all run well
in containers, and the existing `docker-jitsi-meet`_ project has
worked out how to run all of them together.

The web service is written as a React Javascript application, a
framework which some OpenDev contributors already have familiarity, so
potential future customization is possible.

We will configure jitsi-meet to use our existing etherpad server for
shared document editing.  We will also configure it to use the "room
name" as the etherpad name.  This will make it easy to prepare
etherpads before conferences and refer to them afterwords.  Users
would visit a url such as
``https://meetpad.opendev.org/zuulv5-planning`` for a video
conference, and that would use the etherpad at
``https://etherpad.openstack.org/p/zuulv5-planning``.  The predictable
URLs will make it easy to find meetings and find their notes after
they are complete.

We will also make a small change to the jitsi-meet software so that
the etherpad is automatically opened when a user joins a room.  Users
can still close and re-open the etherpad at will in order to engage in
screen sharing or other uses of the main window, but since the service
is etherpad focused, that should be the initial state.

.. _docker-jitsi-meet: https://github.com/jitsi/docker-jitsi-meet

Assignee(s)
-----------

Primary assignee:
  * corvus

Gerrit Topic
------------

Use Gerrit topic ``meetpad`` for all patches related to this spec.

.. code-block:: bash

    git-review -t meetpad

Work Items
----------

* Create meetpad.opendev.org server
* Build custom jitsi-meet web container image (with
  open-etherpad-on-startup patch)
* Deploy full stack with docker-compose and letsencrypt

Repositories
------------

None, unless we are unable to upstream our customizations to
jitsi-meet, in which case we may need to carry a thin fork.

Servers
-------

meetpad.opendev.org VM.

DNS Entries
-----------

meetpad.opendev.org A record and associated letsencrypt records.

Documentation
-------------

We will need a new page in the system-config documentation describing
the deployment.  If we find this satisfactory, we should work with
OpenDev communities to promote its use during virtual events.  We may
want to integrate it with ptgbot.

Security
--------

This server will have no special access to privileged data, no special
access to the etherpad service, and like etherpad itself, all usage
will be anonymous.

Testing
-------

Typical system-config deployment testing should be feasible on this
stack.

Dependencies
============

None.
