::

  Copyright <YEARS> <HOLDER>  <--UPDATE THESE

  This work is licensed under a Creative Commons Attribution 3.0
  Unported License.
  http://creativecommons.org/licenses/by/3.0/legalcode

..
  This template should be in ReSTructured text. Please do not delete
  any of the sections in this template.  If you have nothing to say
  for a whole section, just write: "None". For help with syntax, see
  http://sphinx-doc.org/rest.html To test out your formatting, see
  http://www.tele3.cz/jbar/rest/rest.html

===============================
The Title of Your Specification
===============================

Include the URL of your StoryBoard story:

https://storyboard.openstack.org/...

Introduction paragraph -- why are we doing anything?

Problem Description
===================

A detailed description of the problem.

Proposed Change
===============

Here is where you cover the change you propose to make in detail. How do you
propose to solve this problem?

If this is one part of a larger effort make it clear where this piece ends. In
other words, what's the scope of this effort?

Alternatives
------------

This is an optional section, where it does apply we'd just like a demonstration
that some thought has been put into why the proposed approach is the best one.

Implementation
==============

Assignee(s)
-----------

- <IRC nick or "None" if intended for the Help Wanted section>
- <IRC nick of additional assignee if any, repeat as needed>

Gerrit Topic
------------

Use Gerrit topic "<topic_name>" for all patches related to this spec.

.. code-block:: bash

    git-review -t <topic_name>

Work Items
----------

Work items or tasks -- break the feature up into the things that need to be
done to implement it. Those parts might end up being done by different people,
but we're mostly trying to understand the timeline for implementation.

Repositories
------------

Will any new git repositories need to be created?

Servers
-------

Will any new servers need to be created?  What existing servers will
be affected?

DNS Entries
-----------

Will any other DNS entries need to be created or updated?

Backups
-------

Will anything new need to be backed up, or will existing backups
need changing/deleting?

Documentation
-------------

Will this require a documentation change?  If so, which documents?
Will it impact developer workflow?  Will additional communication need
to be made?

Security
--------

Does this introduce any additional security risks, or are there
security-related considerations which should be discussed?

Testing
-------

What tests will be available or need to be constructed in order to
validate this?  Unit/functional tests, development
environments/servers, etc.

Dependencies
============

- Include specific references to specs and/or stories in infra, or in
  other projects, that this one either depends on or is related to.

- Does this feature require any new library or program dependencies
  not already in use?

- Does it require a new puppet module?
